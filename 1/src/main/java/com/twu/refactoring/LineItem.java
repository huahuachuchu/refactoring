package com.twu.refactoring;

public class LineItem {
	private final String description;
	private final double price;
	private final int quantity;

	public LineItem(String description, double price, int quantity) {
		super();
		this.description = description;
		this.price = price;
		this.quantity = quantity;
	}

	 String getDescription() {
		return description;
	}

	 double getPrice() {
		return price;
	}

	 int getQuantity() {
		return quantity;
	}

    double totalAmount() {
        return price * quantity;
    }
    double getSalesTax() {
		return totalAmount() * .10;
	}
}