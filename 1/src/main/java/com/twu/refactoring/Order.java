package com.twu.refactoring;

import java.util.List;

public class Order {
    private final String name;
    private final String address;
    private final List<LineItem> items;

    public Order(String name, String address, List<LineItem> items) {
        this.name = name;
        this.address = address;
        this.items = items;
    }

     String getCustomerName() {
        return name;
    }

     String getCustomerAddress() {
        return address;
    }

     List<LineItem> getLineItems() {
        return items;
    }
}
